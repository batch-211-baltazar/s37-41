const Course = require("../models/Course");
const User = require("../models/User");


// module.exports.addCourse = (reqBody) =>{
	// let newCourse = new Course({
	// 	name : reqBody.name,
	// 	description : reqBody.description,
	// 	price : reqBody.price
	// })

// 	return newCourse.save().then((user,error)=>{
// 		if(error){
// 			return false;
// 		}else{
// 			return true;
// 		};
// 	});
// };



/*

s39 Activity Instructions:
Refactor the addCourse controller method to implement admin authentication for creating a course.
Steps:
1. Create a conditional statement that will check if the user is an administrator.
2. Create a new Course object using the mongoose model and the information from the request body and the id from the header
3. Save the new User to the database

*/


// module.exports.addCourse = (data) =>{

// 	console.log(data)

// 	return User.find(data.token).then(result=>{
// 			if(data.isAdmin==false){
// 				return false
// 			}else{
// 				let newCourse = new Course({
// 				name : data.course.name,
// 				description :data.course.description,
// 				price : data.course.price
// 				})

// 		console.log(newCourse);
// 		return newCourse.save().then((user,error)=>{
// 				return true
// 			})

// 			}})}


//Solution 1
/*
module.exports.addCourse = (reqBody) => {

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price : reqBody.price
	});

	if(reqBody.isAdmin){

		return newCourse.save().then((course, error) => {
			if (error) {
				return false;
			}else{
				return true;
			};
		});
	}else{
		return false;
	}
};

*/


//Solution 2

module.exports.addCourse = (data) => {

	console.log(data);

	if(data.isAdmin){

		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});

		console.log(newCourse);
		return newCourse.save().then((course, error) => {

		if (error) {

			return false;

		} else {

			return true;

		};

	});

	}else{
		return false;
	};
};



	//Academic Break Reading List
/*
"In your s37-s41 project:
1. Create a route for retrieving all the courses.
2. Create a controller method for retrieving all the courses.
3. Process a GET request at the ""/all"" route using postman to retrieve all the courses.
4. Retrieve all the courses from the database

5. Create a route for retrieving all the courses.
6. Create a controller method for retrieving all the courses.
7. Process a GET request at the ""/"" route using postman to retrieve all the courses.
8. Retrieve all the courses from the database with the property of ""isActive"" to true"
*/

//Retrieve all courses
/*
	1. Retrieve all the courses from  the database
		Model.find({})
*/

module.exports.getAllCourses =()=>{
	return Course.find({}).then(result=>{
		return result;
	})
}

//Retrieve all Active Courses
/*
	1. Retrieve all the courses from the database with the property of "isActive" to true

*/
module.exports.getAllActive =()=>{
	return Course.find({isActive: true}).then(result=>{
			return result
})}

/*

"In your s37-s41 project:
1. Create a route for retrieving the details of a specific course.
2. Create a controller method for retrieving a specific course.
3. Process a GET request at the ""/:courseId"" route using postman to retrieve all the active courses.
4. Retrieve the course that matches the course ID provided from the URL"

*/

//Retrieving a specific course
/*
	1. Retrieve the course that matches the course ID provided from the URL
*/

module.exports.getCourse = (reqParams)=>{
	return Course.findById(reqParams.courseId).then(result=>{
		return result;
	});
};

//Update a course
/*
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/

module.exports.updateCourse = (reqParams,reqBody) =>{
	let updatedCourse ={
		name: reqBody.name,
		description: reqBody.description,
		price:reqBody.price
	};
	return Course.findByIdAndUpdate(reqParams.courseId,updatedCourse).then((course,error)=>{
		if(error){
			return false;
		}else{
			return true;
		};
	});
};

//s40 Activity: Archive Course

module.exports.archiveCourse = (reqParams,reqBody) =>{
	let archiveCourse ={
		isActive : reqBody.isActive
	};
	return Course.findByIdAndUpdate(reqParams.courseId,archiveCourse).then((course,error)=>{
		if(error){
			return false;
		}else{
			return true;
		};
	});
};